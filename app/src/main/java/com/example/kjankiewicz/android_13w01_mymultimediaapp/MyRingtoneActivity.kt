package com.example.kjankiewicz.android_13w01_mymultimediaapp

import android.app.Activity
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_my_ringtone.*


class MyRingtoneActivity : AppCompatActivity() {
    private var currentRingtone: Ringtone? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_ringtone)

        playRingtoneButton.setOnClickListener {
            val ringtoneUri: Uri = when {
                ringRingtoneRadioButton.isChecked -> RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
                notificationRingtoneRadioButton.isChecked -> RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                else -> RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
            }

            if (currentRingtone != null && currentRingtone!!.isPlaying) currentRingtone!!.stop()

            currentRingtone = RingtoneManager.getRingtone(applicationContext, ringtoneUri)

            if (currentRingtone != null) currentRingtone!!.play()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_ringtone, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (currentRingtone != null && currentRingtone!!.isPlaying)
            currentRingtone!!.stop()
        val id = item.itemId
        if (id == R.id.ringtone_picker) {
            val i = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
            i.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
            i.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            when {
                ringRingtoneRadioButton.isChecked -> {
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone for calls:")
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE)
                    startActivityForResult(i, ACTIVITY_SET_RING_RINGTONE)
                }
                notificationRingtoneRadioButton.isChecked -> {
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone for notifications:")
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
                    startActivityForResult(i, ACTIVITY_SET_NOTIFICATION_RINGTONE)
                }
                else -> {
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select ringtone for alarms:")
                    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
                    startActivityForResult(i, ACTIVITY_SET_ALARM_RINGTONE)
                }
            }

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val uri = data!!
                    .getParcelableExtra<Uri>(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)
            when (requestCode) {
                ACTIVITY_SET_RING_RINGTONE -> RingtoneManager.setActualDefaultRingtoneUri(this,
                        RingtoneManager.TYPE_RINGTONE, uri)
                ACTIVITY_SET_NOTIFICATION_RINGTONE -> RingtoneManager.setActualDefaultRingtoneUri(this,
                        RingtoneManager.TYPE_NOTIFICATION, uri)
                ACTIVITY_SET_ALARM_RINGTONE -> RingtoneManager.setActualDefaultRingtoneUri(this,
                        RingtoneManager.TYPE_ALARM, uri)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    public override fun onPause() {
        if (currentRingtone != null && currentRingtone!!.isPlaying)
            currentRingtone!!.stop()
        super.onPause()
    }

    companion object {
        internal const val ACTIVITY_SET_RING_RINGTONE = 1972
        internal const val ACTIVITY_SET_NOTIFICATION_RINGTONE = 1973
        internal const val ACTIVITY_SET_ALARM_RINGTONE = 1974
    }
}
