@file:Suppress("DEPRECATION")

package com.example.kjankiewicz.android_13w01_mymultimediaapp

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Camera
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Surface
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.FrameLayout
import android.widget.Toast

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat

import kotlinx.android.synthetic.main.activity_my_camera.*
import java.util.*

class MyCameraActivity : AppCompatActivity() {
    private var myCamera: Camera? = null
    private var myPreview: CameraPreview? = null
    private var myMediaRecorder: MediaRecorder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_camera)

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "Your device does not have camera.",
                    Toast.LENGTH_LONG).show()
            finish()
        }

        try {
            myCamera = Camera.open()
        } catch (e: Exception) {
            Toast.makeText(this, "Camera is not available", Toast.LENGTH_LONG).show()
            finish()
        }

        myPreview = CameraPreview(this, myCamera!!)
        val preview = findViewById<FrameLayout>(R.id.camera_preview)
        preview.addView(myPreview)

        myPreview!!.setOnClickListener {
            if (myCamera != null)
                myCamera!!.startPreview()
        }

        // for pictures

        val myPicture = Camera.PictureCallback { data, _ ->

            val pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE)
            if (pictureFile == null) {
                Log.d(TAG, "Error creating media file, check storage permissions")
            }

            try {
                val fos = FileOutputStream(pictureFile)
                fos.write(data)
                fos.close()
            } catch (e: FileNotFoundException) {
                Log.d(TAG, "File not found: " + e.message)
            } catch (e: IOException) {
                Log.d(TAG, "Error accessing file: " + e.message)
            }
        }

        // Add a listener to the Capture button
        takePictureButton.setOnClickListener { myCamera!!.takePicture(null, null, myPicture) }

        // for videos

        togglePictureOrVideoButton.textOff = "VC Stopped"
        togglePictureOrVideoButton.textOn = "VC Recording"
        togglePictureOrVideoButton.invalidate()

        takeVideoButton.setOnClickListener {
            if (togglePictureOrVideoButton.isChecked) {
                myMediaRecorder!!.stop()
                releaseMediaRecorder()
                takePictureButton.isEnabled = true
                togglePictureOrVideoButton.isChecked = false
            } else {
                togglePictureOrVideoButton.isChecked = true
                takePictureButton.isEnabled = false

                myMediaRecorder = MediaRecorder()

                myCamera!!.unlock()
                myMediaRecorder!!.setCamera(myCamera)

                myMediaRecorder!!.setAudioSource(
                        MediaRecorder.AudioSource.CAMCORDER)
                myMediaRecorder!!.setVideoSource(
                        MediaRecorder.VideoSource.CAMERA)

                myMediaRecorder!!.setProfile(
                        CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH))

                myMediaRecorder!!.setOutputFile(
                        getOutputMediaFile(MEDIA_TYPE_VIDEO)!!.toString())

                myMediaRecorder!!.setPreviewDisplay(myPreview!!.holder.surface)

                try {
                    myMediaRecorder!!.prepare()
                } catch (e: IllegalStateException) {
                    Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.message)
                    releaseMediaRecorder()
                } catch (e: IOException) {
                    Log.d(TAG, "IOException preparing MediaRecorder: " + e.message)
                    releaseMediaRecorder()
                }

                myMediaRecorder!!.start()
            }
        }

    }

    private fun releaseMediaRecorder() {
        if (myMediaRecorder != null) {
            myMediaRecorder!!.reset()
            myMediaRecorder!!.release()
            myMediaRecorder = null
            myCamera!!.lock()
        }
    }

    private fun releaseCamera() {
        if (myCamera != null) {
            myCamera!!.release()        // release the camera for other applications
            myCamera = null
        }
    }

    override fun onPause() {
        super.onPause()
        releaseMediaRecorder()
        releaseCamera()
    }


    inner class CameraPreview(context: Context, private val mCamera: Camera) : SurfaceView(context), SurfaceHolder.Callback {

        private val mHolder: SurfaceHolder = holder

        init {

            mHolder.addCallback(this)

            setCameraDisplayOrientation(this@MyCameraActivity, 0, mCamera)
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            try {
                mCamera.setPreviewDisplay(holder)
                mCamera.startPreview()
            } catch (e: IOException) {
                Log.d(TAG, "Error setting camera preview: " + e.message)
            }

        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {}

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

            if (mHolder.surface == null) {
                return
            }

            try {
                mCamera.stopPreview()
            } catch (ignored: Exception) {
            }

            try {
                mCamera.setPreviewDisplay(mHolder)
                mCamera.startPreview()
            } catch (e: Exception) {
                Log.d(TAG, "Error starting camera preview: " + e.message)
            }

        }
    }

    companion object {

        const val MEDIA_TYPE_IMAGE = 1
        const val MEDIA_TYPE_VIDEO = 2

        internal const val TAG = "MyCameraActivity"

        private fun getOutputMediaFile(type: Int): File? {
            val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), TAG)

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.e(TAG, "failed to create directory")
                    return null
                }
            }

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val mediaFile: File
            mediaFile = when (type) {
                MEDIA_TYPE_IMAGE -> File(mediaStorageDir.path + File.separator +
                        "IMG_" + timeStamp + ".jpg")
                MEDIA_TYPE_VIDEO -> File(mediaStorageDir.path + File.separator +
                        "VID_" + timeStamp + ".mp4")
                else -> return null
            }
            return mediaFile
        }

        /*fun getOutputMediaFileUri(type: Int): Uri? {
            val file = getOutputMediaFile(type)
            return if (file != null)
                Uri.fromFile(file)
            else
                null
        }*/

        fun setCameraDisplayOrientation(
                activity: AppCompatActivity,
                cameraId: Int,
                camera: android.hardware.Camera) {
            val info = android.hardware.Camera.CameraInfo()
            android.hardware.Camera.getCameraInfo(cameraId, info)
            val rotation = activity.windowManager.defaultDisplay
                    .rotation
            var degrees = 0
            when (rotation) {
                Surface.ROTATION_0 -> degrees = 0
                Surface.ROTATION_90 -> degrees = 90
                Surface.ROTATION_180 -> degrees = 180
                Surface.ROTATION_270 -> degrees = 270
            }

            var result: Int
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360
                result = (360 - result) % 360  // compensate the mirror
            } else {  // back-facing
                result = (info.orientation - degrees + 360) % 360
            }
            camera.setDisplayOrientation(result)
        }
    }
}
