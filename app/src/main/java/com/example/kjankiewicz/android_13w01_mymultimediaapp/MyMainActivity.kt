package com.example.kjankiewicz.android_13w01_mymultimediaapp

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.MediaController
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_main.*


class MyMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        val intent: Intent
        when (id) {
            R.id.activity_my_audio_manager -> {
                intent = Intent(this, MyAudioManagerActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.activity_my_ringtone -> {
                intent = Intent(this, MyRingtoneActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.activity_my_media_player -> {
                intent = Intent(this, MyMediaPlayerActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.activity_my_camera -> {
                useCamera()
                return true
            }
            R.id.activity_my_camera2 -> {
                useCamera2()
                return true
            }
            R.id.external_camera -> {
                useExternalCameraForImage()
                return true
            }
            R.id.external_video_camera -> {
                useExternalCameraForVideo()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun useCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"),
                    PERMISSIONS_REQUEST_CAMERA)
        } else {
            val intent = Intent(this, MyCameraActivity::class.java)
            startActivity(intent)
        }
    }

    private fun useCamera2() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"),
                    PERMISSIONS_REQUEST_CAMERA2)
        } else {
            val intent = Intent(this, MyCamera2Activity::class.java)
            startActivity(intent)
        }
    }

    private fun useExternalCameraForImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.CAMERA"),
                    PERMISSIONS_REQUEST_EXTERNAL_CAMERA)
        } else {
            intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
            }
        }
    }

    private fun useExternalCameraForVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission("android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf("android.permission.CAMERA"),
                    PERMISSIONS_REQUEST_EXTERNAL_VIDEO_CAMERA)
        } else {
            intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                useCamera()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, camera cannot be accessed",
                        Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == PERMISSIONS_REQUEST_CAMERA2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                useCamera2()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, camera cannot be accessed",
                        Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == PERMISSIONS_REQUEST_EXTERNAL_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                useExternalCameraForImage()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, external camera (for image) cannot be accessed",
                        Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == PERMISSIONS_REQUEST_EXTERNAL_VIDEO_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                useExternalCameraForVideo()
            } else {
                Toast.makeText(this,
                        "Until you grant the permission, external camera (for video) cannot be accessed",
                        Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int,
                                  returnIntent: Intent?) {
        // data is null - Android received file name - app knows it too
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val extras = returnIntent!!.extras
                    val imageBitmap: Bitmap?
                    if (extras != null) {
                        imageBitmap = extras.get("data") as Bitmap
                        capturedPhotoImageView.setImageBitmap(imageBitmap)
                    }
                }
                Activity.RESULT_CANCELED -> {
                }
                else -> {
                }
            }// User cancelled the image capture
            // Image capture failed, advise user
        }
        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val videoUri = returnIntent!!.data
                    capturedVideoView.setVideoURI(videoUri)
                    val mediaController = MediaController(this, true)
                    mediaController.isEnabled = false
                    capturedVideoView.setMediaController(mediaController)
                    capturedVideoView.setOnPreparedListener { mediaController.isEnabled = true }
                }
                Activity.RESULT_CANCELED -> {
                }
                else -> {
                }
            }// User cancelled the video capture
            // Video capture failed, advise user
        }
    }

    companion object {

        private const val CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100
        private const val CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200

        private const val PERMISSIONS_REQUEST_EXTERNAL_CAMERA = 1
        private const val PERMISSIONS_REQUEST_EXTERNAL_VIDEO_CAMERA = 2
        private const val PERMISSIONS_REQUEST_CAMERA = 3
        private const val PERMISSIONS_REQUEST_CAMERA2 = 4
    }
}
