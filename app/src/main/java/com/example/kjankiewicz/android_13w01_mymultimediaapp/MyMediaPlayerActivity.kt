package com.example.kjankiewicz.android_13w01_mymultimediaapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ImageButton
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import kotlinx.android.synthetic.main.activity_my_media_player.*

import java.io.IOException
import android.media.AudioAttributes


class MyMediaPlayerActivity : AppCompatActivity(), MediaPlayer.OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

    private var lastAudioFocusState = AudioFocusState()

    private lateinit var audioManager: AudioManager

    private var mMediaPlayer: MediaPlayer? = null

    private val mHandler = Handler()

    private val seekTime = 5000
    private var currentSongIndex = 0
    private var isRepeat = false

    private val songsNames = arrayOf("ironbutterfly_sample", "jodygrind_sample")
    private val movieFile = "_007ad_qtp"

    // Video Part

    private var myVideoView: VideoView? = null
    private var isRecording = false
    private var mediaRecorder: MediaRecorder? = null
    private var permissionForMediaPlayerAccepted = false
    private val permissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    private val mSetPosition = object : Runnable {

        override fun run() {
            if (mMediaPlayer != null) {
                val fileDuration = mMediaPlayer!!.duration / 1000
                val filePosition = mMediaPlayer!!.currentPosition / 1000
                fileDurationTextView.text = fileDuration.toString()
                filePositionTextView.text = filePosition.toString()
                val progress = filePosition * 100 / fileDuration
                songProgressBar.progress = progress
                mHandler.postDelayed(this, 500)
            }
        }
    }

    private inner class AudioFocusState {
        internal var audioFocusGranted: Boolean = false
        internal var state: Int = 0
        internal var isPlaying: Boolean = false
        internal var volumeLevel: Int = 0
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSIONS_FOR_MEDIAPLAYER -> {
                permissionForMediaPlayerAccepted = true
                for (grantResult in grantResults)
                    permissionForMediaPlayerAccepted = permissionForMediaPlayerAccepted && grantResult == PackageManager.PERMISSION_GRANTED
            }
        }
        if (!permissionForMediaPlayerAccepted) finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_media_player)

        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS_FOR_MEDIAPLAYER)

        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        mMediaPlayer = MediaPlayer()
        mMediaPlayer!!.setOnCompletionListener(this)

        startSong(0, false)

        playButton.setOnClickListener {
            if (mMediaPlayer!!.isPlaying) {
                if (mMediaPlayer != null) {
                    mMediaPlayer!!.pause()
                    setRightPlayIcon()
                }
            } else {
                if (mMediaPlayer != null) {
                    mMediaPlayer!!.start()
                    setRightPlayIcon()
                }
            }
        }

        forwardButton.setOnClickListener {
            val currentPosition = mMediaPlayer!!.currentPosition
            if (currentPosition + seekTime <= mMediaPlayer!!.duration) {
                mMediaPlayer!!.seekTo(currentPosition + seekTime)
            } else {
                mMediaPlayer!!.seekTo(mMediaPlayer!!.duration)
            }
        }

        backwardButton.setOnClickListener {
            val currentPosition = mMediaPlayer!!.currentPosition
            if (currentPosition - seekTime >= 0) {
                mMediaPlayer!!.seekTo(currentPosition - seekTime)
            } else {
                mMediaPlayer!!.seekTo(0)
            }
        }

        nextButton.setOnClickListener {
            currentSongIndex = if (currentSongIndex < songsNames.size - 1)
                currentSongIndex + 1
            else
                0
            startSong(currentSongIndex, mMediaPlayer!!.isPlaying)
        }

        previousButton.setOnClickListener {
            currentSongIndex = if (currentSongIndex > 0)
                currentSongIndex - 1
            else
                songsNames.size - 1
            startSong(currentSongIndex, mMediaPlayer!!.isPlaying)
        }

        repeatButton.setOnClickListener {
            if (isRepeat) {
                isRepeat = false
                Toast.makeText(applicationContext, "Repeat is OFF", Toast.LENGTH_SHORT).show()
            } else {
                isRepeat = true
                Toast.makeText(applicationContext, "Repeat is ON", Toast.LENGTH_SHORT).show()
            }
        }

        // Video part

        myVideoView = findViewById(R.id.myVideoView)
        val mediaController = MediaController(this, true)
        mediaController.isEnabled = false
        myVideoView!!.setMediaController(mediaController)
        val uriPath = "android.resource://$packageName/raw/$movieFile"
        val uri = Uri.parse(uriPath)
        myVideoView!!.setVideoURI(uri)
        myVideoView!!.setOnPreparedListener { mediaController.isEnabled = true }

        // Recorder part

        val btnRecord = findViewById<ImageButton>(R.id.recordButton)

        btnRecord.setOnClickListener {
            if (!isRecording) {
                mediaRecorder = MediaRecorder()

                mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
                mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                mediaRecorder!!.setOutputFile(mRecordedFile)
                mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

                try {
                    mediaRecorder!!.prepare()
                } catch (e: IOException) {
                    Log.e("MediaRecorder", "Can not be prepared")
                    e.printStackTrace()
                }

                mediaRecorder!!.start()
                isRecording = true
            } else {
                if (mediaRecorder != null) {
                    mediaRecorder!!.stop()
                    mediaRecorder!!.reset()
                    mediaRecorder!!.release()
                    mediaRecorder = null
                    myVideoView = findViewById(R.id.myVideoView)
                    myVideoView!!.setVideoPath(mRecordedFile)
                }
            }
        }

    }

    override fun onPause() {
        if (myVideoView != null) {
            if (myVideoView!!.isPlaying)
                myVideoView!!.stopPlayback()
            myVideoView = null
        }
        super.onPause()
    }

    public override fun onDestroy() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying)
                mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
        super.onDestroy()
    }

    private fun setRightPlayIcon() {
        if (mMediaPlayer!!.isPlaying)
            playButton.setImageResource(R.drawable.pause)
        else
            playButton.setImageResource(R.drawable.play)
    }


    private fun startSong(songIndex: Int, andPlay: Boolean) {

        val audioFocusRequestGranted = requestAudioFocus(this, audioManager)

        if (audioFocusRequestGranted) {
            try {
                mMediaPlayer?.let {
                    it.reset()
                    it.setAudioAttributes(AudioAttributes.Builder()
                            .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                            .build())
                    val uriPath = "android.resource://" + packageName +
                            "/raw/" + songsNames[songIndex]
                    val uri = Uri.parse(uriPath)
                    it.setDataSource(this, uri)

                    it.prepare()
                    if (andPlay) it.start()
                }

                fileNameTextView!!.text = songsNames[songIndex]

                setRightPlayIcon()

                with (songProgressBar) {
                    progress = 0
                    max = 100
                }

                mHandler.postDelayed(mSetPosition, 500)


            } catch (e: IllegalStateException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    override fun onCompletion(mp: MediaPlayer) {

        abandonAudioFocus(this, audioManager)

        when {
            isRepeat -> startSong(currentSongIndex, true)
            currentSongIndex < songsNames.size - 1 -> {
                currentSongIndex++
                startSong(currentSongIndex, true)
            }
            else -> setRightPlayIcon()
        }
    }

    /* When Audio Focus Change - Activity is OnAudioFocusChangeListener */
    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                lastAudioFocusState.audioFocusGranted = true
                when (lastAudioFocusState.state) {
                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> if (lastAudioFocusState.isPlaying)
                        mMediaPlayer!!.start()
                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> audioManager.setStreamVolume(
                            AudioManager.STREAM_MUSIC,
                            lastAudioFocusState.volumeLevel, 0)
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> if (mMediaPlayer != null) {
                lastAudioFocusState.isPlaying = mMediaPlayer!!.isPlaying
                if (mMediaPlayer!!.isPlaying) mMediaPlayer!!.pause()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                lastAudioFocusState.volumeLevel = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
                val halfOfVolumeLevel = lastAudioFocusState.volumeLevel / 2
                audioManager.setStreamVolume(
                        AudioManager.STREAM_MUSIC, halfOfVolumeLevel, 0)
            }
            AudioManager.AUDIOFOCUS_LOSS -> {
                if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) {
                    mMediaPlayer!!.stop()
                    setRightPlayIcon()
                }
                abandonAudioFocus(this, audioManager)
            }
        }
        lastAudioFocusState.state = focusChange
    }

    companion object {

        // Recorder Part

        private val mRecordedFile = Environment.getExternalStorageDirectory().absolutePath + "/current_lesson.3gp"


        // Requesting permission
        private const val REQUEST_PERMISSIONS_FOR_MEDIAPLAYER = 1
    }
}
