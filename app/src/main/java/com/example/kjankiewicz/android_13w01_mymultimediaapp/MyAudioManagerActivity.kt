package com.example.kjankiewicz.android_13w01_mymultimediaapp

import android.annotation.TargetApi
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.SparseIntArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast

class MyAudioManagerActivity : AppCompatActivity() {

    private var systemRingVolumeTextView: TextView? = null
    private var ringVolume: Int = 0
    private var ringMaxVolume: Int = 0
    private var ringerMode: Int = 0

    private var silentModeRadioButton: RadioButton? = null
    private var vibrateModeRadioButton: RadioButton? = null
    private var normalModeRadioButton: RadioButton? = null

    private lateinit var mAudioManager: AudioManager
    private var mSoundPool: SoundPool? = null
    private var audioFocusRequestGranted = false
    private var soundPoolStreamsNo = 1

    private lateinit var afChangeListener: AudioManager.OnAudioFocusChangeListener

    private var soundsToButtonsMap = SparseIntArray()
    private var buttonsToSoundsMap = SparseIntArray()

    private var soundButtons = intArrayOf(R.id.button_sound1, R.id.button_sound2, R.id.button_sound3, R.id.button_sound4, R.id.button_sound5, R.id.button_sound6, R.id.button_sound7, R.id.button_sound8, R.id.button_sound9)
    private var soundResources = intArrayOf(R.raw.sound1, R.raw.sound2, R.raw.sound3, R.raw.sound4, R.raw.sound5, R.raw.sound6, R.raw.sound7, R.raw.sound8, R.raw.sound9)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_audio_manager)

        mAudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        ringMaxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING)

        systemRingVolumeTextView = findViewById(R.id.systemRingVolumeTextView)

        val buttonUp = findViewById<Button>(R.id.buttonUp)
        val buttonDown = findViewById<Button>(R.id.buttonDown)

        silentModeRadioButton = findViewById(R.id.silentModeRadioButton)
        vibrateModeRadioButton = findViewById(R.id.vibrateModeRadioButton)
        normalModeRadioButton = findViewById(R.id.normalModeRadioButton)

        /* Prepare Audio Focus Change Listener */
        afChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
            when (focusChange) {
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> mSoundPool!!.autoPause()
                AudioManager.AUDIOFOCUS_GAIN -> mSoundPool!!.autoResume()
                AudioManager.AUDIOFOCUS_LOSS -> abandonAudioFocus(afChangeListener, mAudioManager)

            }
        }

        /* Request Audio Focus */
        audioFocusRequestGranted = requestAudioFocus(afChangeListener, mAudioManager)


        /* Volume UP */
        buttonUp.setOnClickListener {
            if (ringerMode == AudioManager.RINGER_MODE_SILENT && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Toast.makeText(this,
                        "Change Do Not Disturb can " +
                                "be done only when your API is smaller then 23 or " +
                                "when your app is system app. Sorry", Toast.LENGTH_LONG).show()
            }
            if (ringVolume < mAudioManager.getStreamMaxVolume(
                            AudioManager.STREAM_RING)) {
                mAudioManager.setStreamVolume(
                        AudioManager.STREAM_RING,
                        ringVolume + 1, AudioManager.FLAG_PLAY_SOUND)
                displayCurrentRingVolume()
                displayCurrentRingerMode()
            }
        }

        /* Volume DOWN */
        buttonDown.setOnClickListener {
            if (ringerMode == AudioManager.RINGER_MODE_SILENT && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Toast.makeText(this,
                        "Change Do Not Disturb can " +
                                "be done only when your API is smaller then 23 or " +
                                "when your app is system app. Sorry", Toast.LENGTH_LONG).show()
            }
            if (ringVolume > 0) {
                mAudioManager.setStreamVolume(
                        AudioManager.STREAM_RING,
                        ringVolume - 1, AudioManager.FLAG_PLAY_SOUND)
                displayCurrentRingVolume()
                displayCurrentRingerMode()
            }
        }

        /* Set Ringer Mode */
        val changeModeListener = View.OnClickListener { v ->
            if ((v as RadioButton).isChecked) {
                when (v.getId()) {
                    R.id.silentModeRadioButton -> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        mAudioManager.ringerMode = AudioManager.RINGER_MODE_SILENT
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Toast.makeText(this@MyAudioManagerActivity,
                                "Change Do Not Disturb (RINGER_MODE_SILENT) can " +
                                        "be done only when your API is smaller then 23 or " +
                                        "when your app is system app. Sorry", Toast.LENGTH_LONG).show()
                    }
                    R.id.vibrateModeRadioButton -> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        mAudioManager.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Toast.makeText(this@MyAudioManagerActivity,
                                "Change Do Not Disturb (RINGER_MODE_VIBRATE) can " +
                                        "be done only when your API is smaller then 23 or " +
                                        "when your app is system app. Sorry", Toast.LENGTH_LONG).show()
                    }
                    R.id.normalModeRadioButton -> if (ringerMode == AudioManager.RINGER_MODE_SILENT) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                            mAudioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Toast.makeText(this@MyAudioManagerActivity,
                                    "Change Do Not Disturb can " +
                                            "be done only when your API is smaller then 23 or " +
                                            "when your app is system app. Sorry", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        mAudioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                    }
                }
            }
            displayCurrentRingerMode()
        }

        silentModeRadioButton!!.setOnClickListener(changeModeListener)
        vibrateModeRadioButton!!.setOnClickListener(changeModeListener)
        normalModeRadioButton!!.setOnClickListener(changeModeListener)

        displayCurrentRingVolume()
        displayCurrentRingerMode()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_audio_manager, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        when (id) {
            R.id.set_1_stream -> {
                soundPoolStreamsNo = 1
                return true
            }
            R.id.set_9_streams -> {
                soundPoolStreamsNo = 9
                return true
            }
            R.id.reloadSoundPool -> {
                releaseSoundPool()
                loadSoundPool()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    public override fun onResume() {
        super.onResume()
        loadSoundPool()
    }

    public override fun onPause() {
        releaseSoundPool()
        super.onPause()
    }

    /* Release SoundPool */
    private fun releaseSoundPool() {
        if (mSoundPool != null) {
            for (i in 0..8)
                mSoundPool!!.unload(buttonsToSoundsMap.get(soundButtons[i]))
            mSoundPool!!.release()
            mSoundPool = null
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun createNewSoundPool() {
        val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
        mSoundPool = SoundPool.Builder()
                .setMaxStreams(soundPoolStreamsNo)
                .setAudioAttributes(attributes)
                .build()
    }

    @Suppress("DEPRECATION")
    private fun oldCreateSoundPool() {
        mSoundPool = SoundPool(soundPoolStreamsNo, AudioManager.STREAM_MUSIC, 0)
    }

    /* Load Sounds to Sound Pool */
    private fun loadSoundPool() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool()
        } else {
            oldCreateSoundPool()
        }

        mSoundPool?.setOnLoadCompleteListener { _, sampleId, status ->
            if (status == 0)
                findViewById<View>(soundsToButtonsMap.get(sampleId)).isEnabled = true
        }

        for (i in 0..8) {
            val buttonPlay = findViewById<Button>(soundButtons[i])

            buttonPlay.isEnabled = false

            val mSoundId = mSoundPool!!.load(applicationContext, soundResources[i], 1)

            soundsToButtonsMap.put(mSoundId, soundButtons[i])
            buttonsToSoundsMap.put(soundButtons[i], mSoundId)

            buttonPlay.setOnClickListener { v ->
                val soundVolume = ringVolume.toFloat() / ringMaxVolume.toFloat()
                if (audioFocusRequestGranted)
                    mSoundPool!!.play(buttonsToSoundsMap.get(v.id),
                            soundVolume, //left
                            soundVolume, //right
                            1, 0, 1f)
            }
        }
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        // Save the user's current game state
        savedInstanceState.putInt("STREAMS_NO", soundPoolStreamsNo)

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState)
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState)

        soundPoolStreamsNo = savedInstanceState.getInt("STREAMS_NO")
    }

    private fun displayCurrentRingVolume() {
        ringVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING)
        systemRingVolumeTextView!!.text = ringVolume.toString()
    }

    private fun displayCurrentRingerMode() {
        ringerMode = mAudioManager.ringerMode

        when (ringerMode) {
            AudioManager.RINGER_MODE_SILENT -> silentModeRadioButton!!.isChecked = true
            AudioManager.RINGER_MODE_VIBRATE -> vibrateModeRadioButton!!.isChecked = true
            AudioManager.RINGER_MODE_NORMAL -> normalModeRadioButton!!.isChecked = true
        }
    }

}
